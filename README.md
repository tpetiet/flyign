#flyIGN

flyIGN is a program which allows to manage the booking of flights.

You need to install Java and Eclipse to run the code. If you want to contribute you will also have to create a Gitlab acount and clone the public project.

#Description

It allows to :

Create flights between two airports.
Add/Cancel passengers.
Choose the most economical plane for a flight.
Provide a summary of the fligt that you can visualize.

#Usage

If you just want to launch the project : *tap 1 if you want to visualize the flights of the day after (directly in the console)
                                         *tap 2 if you want to visualize the destinations served by an airport, then choose your airport (on a panel)
                                         *tap 3 if you want to go to the day after
                                         *tap 4 if you want to leave
                                         
You can watch an example of someone using the program thanks to a video you can ask for by emailing us.

WARNING : Please don't close the notice board (panel), it will update automatically.

#Suport

If you need help you can send an email to : arthur.vignais@ensg.eu, thibault.petiet@ensg.eu, aurore.alarcon@ensg.eu.

#Contributing and Project Status

You can go further by generating a reservation system which is not random and create a user-friendly interface.

#License/Authors

Arthur Vignais, Thibault Petiet, Aurore Alarcon. Open Source
