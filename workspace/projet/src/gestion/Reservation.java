package gestion;

//Imports


import batiments.*;
import myDate.MyDate;


/**
 * 
 * @author Arthur et Thibault
 *
 */

public class Reservation {
		
	//Declarations des attributs
	
	private String id;
	private MyDate date;
	private Aeroport aeDep;
	private Aeroport aeArr;
	private int nbPassagers;
	private Vol vol;
	
	//Constructeurs
	
	public Reservation(String id, MyDate date, Aeroport aeDep, Aeroport aeArr, int nbPassagers) {
		this.id=id;
		this.date=date;
		this.aeDep=aeDep;
		this.aeArr=aeArr;
		this.nbPassagers=nbPassagers;
	}
	
	public Reservation(String id, MyDate date, Aeroport aeDep, Aeroport aeArr, int nbPassagers, Vol vol) {
		this.id=id;
		this.date=date;
		this.aeDep=aeDep;
		this.aeArr=aeArr;
		this.nbPassagers=nbPassagers;
		this.setVol(vol);
	}
	
	//Getter et Setter
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public MyDate getDate() {
		return date;
	}

	public void setDate(MyDate date) {
		this.date = date;
	}

	public Aeroport getAeDep() {
		return aeDep;
	}

	public void setAeDep(Aeroport aeDep) {
		this.aeDep = aeDep;
	}

	public Aeroport getAeArr() {
		return aeArr;
	}

	public void setAeArr(Aeroport aeArr) {
		this.aeArr = aeArr;
	}

	public int getNbPassagers() {
		return nbPassagers;
	}

	public void setNbPassagers(int nbPassagers) {
		this.nbPassagers = nbPassagers;
	}
	
	public Vol getVol() {
		return vol;
	}

	public void setVol(Vol vol) {
		this.vol = vol;
	}
	

}