package gestion;

//Imports

import java.util.ArrayList;


import avion.*;
import batiments.*;
import myDate.MyDate;

/**
 * 
 * @author Thibault
 * Cette classe permet de gerer les vols en leur affectant des avions et les remplissant.
 *
 */

public class Vol {
	
	//Declaration des attributs
	
	private String id;
	private int nb_passagers;
	private Aeroport ae_depart;
	private Aeroport ae_arrivee;
	private Avion avion;
	private MyDate date;
	
	//Constructeurs
	
	
	public Vol(String nom, int nombre_passager, Aeroport aeroport_depart, Aeroport aeroport_arrivee, MyDate date) {
		this.setId(nom);
		this.setNb_passagers(nombre_passager);
		this.ae_depart = aeroport_depart;
		this.ae_arrivee = aeroport_arrivee;
		this.setDate(date);
	}
	
	public Vol() {
	}
	
	public Vol(int nombre_passager, Aeroport aeroport_depart, Aeroport aeroport_arrivee, MyDate date_depart) {
		this.setNb_passagers(nombre_passager);
		this.ae_depart = aeroport_depart;
		this.ae_arrivee = aeroport_arrivee;
		this.setDate(date_depart);
	}
	

	public Vol (String nom, int nombre_passager, Aeroport aeroport_depart, Aeroport aeroport_arrivee, Avion avion, MyDate date) {
		this.setId(nom);
		this.setNb_passagers(nombre_passager);
		this.ae_depart = aeroport_depart;
		this.ae_arrivee = aeroport_arrivee;
		this.setAvion(avion);
		this.setDate(date);
	}
	
	//Getters et Setters
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Aeroport getAe_depart() {
		return ae_depart;
	}

	public void setAe_depart(Aeroport ae_depart) {
		this.ae_depart = ae_depart;
	}

	public Aeroport getAe_arrivee() {
		return ae_arrivee;
	}

	public void setAe_arrivee(Aeroport ae_arrivee) {
		this.ae_arrivee = ae_arrivee;
	}

	public int getNb_passagers() {
		return nb_passagers;
	}
	
	public void setNb_passagers(int nb_passagers) {
		this.nb_passagers = nb_passagers;
	}
	
	public Avion getAvion() {
		return avion;
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}
	
	public MyDate getDate() {
		return date;
	}

	public void setDate(MyDate date_dep) {
		this.date = date_dep;
	}
	
	
	//Methodes
	
	/**
	 * Cette methode permet de calculer le taux de remplissage d'un avion
	 * 
	 * @param av un modele d'avion
	 * @return le taux de remplissage de l'avion
	 */
	
	public double tauxRemplissage(Avion av) {
		return ((double)getNb_passagers()/(double)av.getCapacite())*100;	
	}
	
	/**
	 * Cette methode permet d'ajouter des passagers
	 * 
	 * @param nb nombre entier de passagers a ajouter
	 */
	
	public void ajouterPassager(int nb) {
		setNb_passagers(getNb_passagers()+nb);
	}
	
	/**
	 * Cette methode permet de supprimer des passagers
	 * 
	 * @param nb nombre entier de passagers a supprimer
	 */
	
	public void supprimerPassager(int nb) {
		setNb_passagers(getNb_passagers()-nb);
	}
	
	/**
	 * Cette methode calcul un cout d un avion pour un vol donne (plus le cout est faible et plus l avion est efficace pour ce vol)
	 * 
	 * @param d distance parcouru entre les 2 aeroports
	 * @return le cout du vol avec l avion recu en parametre
	 */
	
	public double cout(Avion av) {
		double d = Aeroport.calcul_distance(this.ae_depart, this.ae_arrivee);
		double p = av.depenseCarburant(d)/this.tauxRemplissage(av);
		return p;
	}
	
	/**
	 * Methode permettant de choisir l'avion le plus adapte au vol de sorte a minimiser les couts
	 * 
	 * @param v le vol auquel on doit affecter l'avion
	 * @return l avion choisi pour le vol
	 */
	
	public static Avion choisir_avion(Vol v) {
		double dist = Aeroport.calcul_distance(v.ae_depart, v.ae_arrivee);
		ArrayList<Avion> lst_av= v.ae_depart.getAvions_presents();//On recupere une liste avec tous les avions possibles presents dans l'aeroport
		Avion av_choisi = null;
		int i_choisi = -1;
		for (int i = 0; i<lst_av.size(); i++) {
			if ( ( lst_av.get(i).getCapacite() > v.getNb_passagers() ) && ( lst_av.get(i).getAutonomie()>dist ) ) { //Si l'avion parcouru initialement a suffisament d'autonomie et de capacite en terme de passagers
				if (av_choisi==null){
					av_choisi=lst_av.get(i);i_choisi=i; //On le choisit
				}
				else if (v.cout(av_choisi)>v.cout(lst_av.get(i))){
					//on choisit, parmi les avions disponibles, celui qui depense le moins et qui est le plus rempli.
					av_choisi=lst_av.get(i);i_choisi=i; //On le change par le nouvel avion plus performant
				}
			}
		}
		
		if (av_choisi != null) { //Si l'avion est choisi on l'enleve de la liste des avions disponibles
			lst_av.remove(i_choisi);
		}

		if (av_choisi == null) {//S'il n'y a pas d'avion disponible, on va en chercher dans les aeroports a proximite
			for (Aeroport ae : v.ae_depart.getAeroports_desservis()) {
				lst_av = ae.getAvions_presents();
				for (int i = 0;i<lst_av.size();i++) {
					if ( ( lst_av.get(i).getCapacite() > v.getNb_passagers() ) && ( lst_av.get(i).getAutonomie()>dist ) ) {
						av_choisi=lst_av.get(i);i_choisi=i;
						lst_av.remove(i_choisi);
						return av_choisi;
					}
				}
			}
		}
		if (av_choisi == null) {//Si on n'a toujours pas d'avion on est obligé de supprimer des passagers
			lst_av= v.ae_depart.getAvions_presents();//On recupere une liste avec tous les avions possibles presents dans l'aeroport
			for (int i = 0; i<lst_av.size(); i++) {
				if ( ( lst_av.get(i).getAutonomie()>dist ) ) { //Si l'avion parcouru initialement a suffisament d'autonomie et de capacite en terme de passagers
					if (av_choisi==null){
						av_choisi=lst_av.get(i);i_choisi=i; //On le choisit
					}
					else if (v.tauxRemplissage(av_choisi)>v.tauxRemplissage(lst_av.get(i))){//On prend cette fois çi le taux de remplissage le plus fort pour supprimer un minimum de passager
						av_choisi=lst_av.get(i);i_choisi=i; //On le change par le nouvel avion plus performant
					}
				}
			}
			if (av_choisi == null) {
				System.out.println(v.id);
			}
			v.supprimerPassager(v.getNb_passagers()-av_choisi.getCapacite());//on supprime des passagers de maniere a ne plus depasser la capacite de l avion
		}
		return av_choisi;
	}

	/**
	 * Cette methode fournit un recapitulatif du vol en donnant ses principales informations
	 * 
	 * @return une chaine de caracteres correspondant au recapitulatif
	 */
	
	public String fournirRecap() {
		double dist = Aeroport.calcul_distance(ae_depart,ae_arrivee);
		String str_ae,str_dist,str_date,str_nbpass,str_av,str_tx;
		str_ae = "Aeroport de Depart : "+this.ae_depart.getId()+" //Aeroport d Arrivee : "+this.ae_arrivee.getId();
		str_dist = " //Distance :"+dist;
		str_date = " //Date :"+this.date.affiche();
		str_nbpass = " //Nombre de passagers :"+this.getNb_passagers();
		if (avion!=null) {
			str_av = " //Avion :"+this.avion.getId();
			str_tx = " //Taux de remplissage :"+this.tauxRemplissage(this.avion);
		}
		else {
			str_av="// ";
			str_tx="// ";
		}
		return str_ae+str_dist+str_date+str_nbpass+str_av+str_tx;
	}
	

}