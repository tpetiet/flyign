package batiments;

//Imports

import java.util.ArrayList;

import avion.*;

import java.lang.Math;

/**
 * @author Thibault
 * 
 */

public class Aeroport {
	
	//Declaration des attributs
	
	private String id;
	private ArrayList<Aeroport> aeroports_desservis;
	private ArrayList<Avion> avions_presents;
	private Coordonnees coor;
 
	
	//Constructeurs
	
	public Aeroport() {
	}

	public Aeroport(String nom, ArrayList<Aeroport> lst_ae, ArrayList<Avion> lst_av,Coordonnees coordonnes) {
		this.setId(nom);
		this.setAeroports_desservis(lst_ae);
		this.setAvions_presents(lst_av);
		this.setCoor(coordonnes);
	}
	
	
	//Getter et Setter
	
	public ArrayList<Aeroport> getAeroports_desservis() {
		return aeroports_desservis;
	}

	public void setAeroports_desservis(ArrayList<Aeroport> aeroports_desservis) {
		this.aeroports_desservis = aeroports_desservis;
	}

	public ArrayList<Avion> getAvions_presents() {
		return avions_presents;
	}

	public void setAvions_presents(ArrayList<Avion> avions_presents) {
		this.avions_presents = avions_presents;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public Coordonnees getCoor() {
		return coor;
	}

	public void setCoor(Coordonnees coor) {
		this.coor = coor;
	}
	
	
	//Methodes
	
	/**
	 * 
	 * Methode qui calcule la distance entre deux aeroports, nous donnera la distance en km des vols.
	 * 
	 * @ param Aeroport ae_dep et ae_arrivee : Les aeroports de depart et arrivee 
	 * @return la distance entre ces deux aeroports selon leur latitude et longitude
	 */
	
	public static double calcul_distance(Aeroport ae_depart,Aeroport ae_arrivee) {
		double lat1=ae_depart.getCoor().getLat() * Math.PI/180;
		double lon1=ae_depart.getCoor().getLon() * Math.PI/180;
		double lat2=ae_arrivee.getCoor().getLat() * Math.PI/180;
		double lon2=ae_arrivee.getCoor().getLon() * Math.PI/180;
		double R_terre = 6371;
		double dist = Math.acos(Math.sin(lat1)*Math.sin(lat2)+Math.cos(lat1)*Math.cos(lat2)*(Math.cos(lon2-lon1)))*R_terre;
		//Formule de Vicenty (Geodesie)
		return dist;
	}
	
	/**
	 * Cette methode genere des aeroports qui contiennent des avions(initialisation)
	 * 
	 * @return une liste d'aeroports
	 */
	
}
