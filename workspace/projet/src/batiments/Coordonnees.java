package batiments;

/**
 * @Author Thibault
 * 
 * Cette classe contient les coordonneees longitude et latitude qui nous serviront a calculer des distances
 */

public class Coordonnees {
	
	//Declaration des attributs
	
	private double lat;
	private double lon;
	
	//Constructeur
	
	public Coordonnees(double latitude,double longitude){
		this.lat = latitude;
		this.lon = longitude;
	}

	//Getter et Setter

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}


	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}
}
