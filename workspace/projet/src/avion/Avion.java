package avion;

/**
 * @author Aurore
 * Cette classe abstraite qui permet de creer des modeles d'avions possedant un identifiant, une autonomie, une capacite en terme de nombre de passagers.
 * 
 */

public abstract class Avion {
	
	//Declaration des attributs
	
	private String id;
	private int autonomie;//en km
	private int capacite;
	
	//Constructeurs
	
	public Avion(String id, int autonomie, int capacite) {
		super();
		this.setId(id);
		this.autonomie = autonomie;
		this.capacite = capacite;	
	}
	
	//Getter et Setter
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public int getAutonomie() {
		return autonomie;
	}
	
	public void setAutonomie(int autonomie) {
		this.autonomie = autonomie;
	}
	
	public int getCapacite() {
		return capacite;
	}
	
	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}
	
	//Methodes 
	
	/**
	 * 
	 * Methode qui permettra de calculer la depense de carburant selon la distance parcouru
	 * @return un double correspondant a la depense de carburant pendant un vol donne
	 */
	
	public abstract double depenseCarburant(double distance_parcourue);

}
