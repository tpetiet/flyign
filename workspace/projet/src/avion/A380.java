package avion;

/**
 * 
 * @author Thibault et Aurore
 * 
 * Classe concrete qui herite de la classe Avion : c'est un modele d'avion avec ses caracteristiques qui lui sont propres
 *
 */

public class A380 extends Avion{
	
//Constructeur
	
	public A380(String id) {
		super(id, 15200, 500);
	}
	
	//Redefinition de methodes

	/**
	 * 
	 * @param distance_parcourue
	 * @return un flottant qui correspond au prix depense pour le carburant pour ce modele d'avion
	 * Cette methode sera utilisee plus tard pour calculer le cout du vol
	 * 
	 */
	
	@Override
	public double depenseCarburant(double distance_parcourue) {
		return distance_parcourue*400000/this.getAutonomie(); //400000 est la taille du reservoir(en litres)
	}
}
