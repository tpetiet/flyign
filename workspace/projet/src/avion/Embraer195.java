package avion;

/**
 * 
 * @author Arthur et Aurore
 * 
 * Classe concrete qui herite de la classe Avion : c'est un modele d'avion avec ses caracteristiques qui lui sont propres
 *
 */

public class Embraer195 extends Avion{
	
	//Constructeur
	
	public Embraer195(String id) {
		super(id, 3000, 140);
	}
	
	//Redefinition de methodes

	/**
	 * 
	 * @param distance_parcourue
	 * @return un double qui correspond au carburant depense pour un vol
	 * Cette methode sera utilisee plus tard pour calculer quel avion consomme le moins pour un vol donne
	 * 
	 */
	
	@Override
	public double depenseCarburant(double distance_parcourue) {
		return distance_parcourue*10000/this.getAutonomie();//16000 est la taille du reservoir(en litres)
	}
}
