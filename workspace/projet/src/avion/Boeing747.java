package avion;

/**
 * 
 * @author Arthur et Aurore
 * 
 * Classe concrete qui herite de la classe Avion : c'est un modele d'avion avec ses caracteristiques qui lui sont propres
 *
 */

public class Boeing747 extends Avion{
	
	//Constructeur
	
	public Boeing747(String id) {
		super(id, 8500, 200);
	}
	
	//Redefinition de methodes

	/**
	 * 
	 * @param distance_parcourue
	 * @return un double qui correspond au carburant depense pour un vol
	 * Cette methode sera utilisee plus tard pour calculer quel avion consomme le moins pour un vol donne
	 * 
	 */
	
	@Override
	public double depenseCarburant(double distance_parcourue) {
		return distance_parcourue*65000/this.getAutonomie();//65000 est la taille du reservoir(en litres)
	}
}