package myDate;

/**
 * Cette classe permet de gerer des dates et passer au jour ou la semaine suivante
 * @author Thibault et Arthur 
 *
 */

public class MyDate {
	
		//Declaration des attributs
	
		private int jour;
		private int mois;
		private int annee;
		
		//Constructeur
		
		public MyDate(int jour, int mois, int annee) {
			this.setJour(jour);
			this.setMois(mois);
			this.setAnnee(annee);
		}
		
		//Getter et Setter
		
		public int getJour() {
			return jour;
		}

		public void setJour(int jour) {
			this.jour = jour;
		}

		public int getMois() {
			return mois;
		}

		public void setMois(int mois) {
			this.mois = mois;
		}

		public int getAnnee() {
			return annee;
		}

		public void setAnnee(int annee) {
			this.annee = annee;
		}
		
		//Methodes
		
		/**
		 * Methode permettant de passer au jour suivant
		 * 
		 * @return la date du jour suivant 
		 */
		
		public MyDate demain() {
			MyDate d = new MyDate(this.jour,this.mois,this.annee);
			if (d.jour<28){
				d.jour+=1;
			}
			else if (d.jour==28 && d.mois==2) { //On prend en compte le mois de fevirer et les annees bisextiles
				if (annee%4!=0) {
					d.jour=1;d.mois+=1;
				}
				else {
					d.jour+=1;
				}
			}
			else if (d.jour==29 && d.mois==2) {
				d.jour=1;d.mois+=1;
			}
			else if (d.jour<30) {
				d.jour+=1;
			}
			else if (d.jour==30 && (d.mois==4 || d.mois==6 || d.mois==9 || d.mois==11)) {
				d.jour=1;d.mois+=1;
			}
			else if (d.jour<31) {
				d.jour+=1;
			}
			else if (d.jour==31 && d.mois!=12) {
				d.jour=1;d.mois+=1;
			}
			else if (d.jour==31 && d.mois==12) {
				d.jour=1;d.mois=1;d.annee+=1;
			}
			return d;
		}
		
		/**
		 * Methode permettant de verifier si une date correspond a celle du jour
		 * 
		 * @param d date
		 * @return true si la date entree est egale a la date du jour
		 */
		
		public Boolean equals(MyDate d) {
			return (this.jour==d.jour && this.mois==d.mois && this.annee==d.annee);
		}
		
		/**
		 * Methode permettant d'obtenir la date du jour precedant
		 * 
		 * @return la date de la veille
		 */
		
		public MyDate hier() {
			MyDate d = new MyDate(this.jour,this.mois,this.annee);
			if (d.jour>1){
				d.jour-=1;
			}
			else if (d.mois==1) {
				d.annee-=1;d.mois=12;d.jour=31;
			}
			else if (d.mois==3) {
				if (annee%4!=0) {
					d.jour=29;d.mois-=1;
				}
				else {
					d.jour=28;d.mois-=1;
				}
			}
			else if (d.mois==5 || d.mois==7 || d.mois==10 || d.mois==12) {
				d.jour=30;d.mois-=1;
			}
			else {
				d.mois-=1;d.jour=31;
			}
			return d;
		}
		
		/**
		 * Methode permettant de passer a la semaine suivante
		 * 
		 * @return la date de la semaine suivante 
		 */
		
		public MyDate semaineSuivante() { //il suffit de passer 7 fois au lendemain
			return this.demain().demain().demain().demain().demain().demain().demain();
		}
		
		/**
		 * Methode qui permet d'afficher la date du jour sous la forme JJ/MM/AAAA ou J/M/AAAA si le jour et le mois sont inferieurs a 10
		 *
		 * @return une chaine de caracteres correspondant a la date
		 */
		
		public String affiche() {
			return jour+"/"+mois+"/"+annee;
		}
}