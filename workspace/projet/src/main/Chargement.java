package main;


import java.util.ArrayList;


import avion.*;
import batiments.*;
import gestion.*;
import myDate.*;

public class Chargement {
	/**
	 * Cette methode permet de verifier qu'un vol s'effectue bien entre deux aeroports donnes
	 * 
	 * @param ae1 l'aeroport theorique de depart
	 * @param ae2 l'aeroport theorique d'arrivee
	 * @param lst la liste des vols a verifier
	 * @return true si on a bien les bons aeroports de depart et d'arrivee
	 */
	
	public static boolean contient(Aeroport ae1,Aeroport ae2,ArrayList<Vol> lst) {
		for (Vol v : lst) {
			if (v.getAe_depart().equals(ae1) && v.getAe_arrivee().equals(ae2)) {return true;}
		}
		return false;
	}
	
	/**
	 * 
	 * Cetter methode permet de creer l'ensemble des vols d'une semaine type
	 * 
	 * @param lst_ae
	 * @param date
	 * @return la liste des vols qui seront effectues dans la semaine
	 */
	
	public static ArrayList<Vol> chargerVol(ArrayList<Aeroport> lst_ae,MyDate date) {
		ArrayList<Vol> lst_volSemaine = new ArrayList<Vol>();
		Aeroport ae1 = null,ae2 = null;
		String id;
		MyDate d = new MyDate(date.getJour(),date.getMois(),date.getAnnee());
		int n,m,z;
		int k = lst_ae.size(); //Nombre d'aeroports
		for (int i=0;i<7;i++) {
			ArrayList<Vol> lst_volJour = new ArrayList<Vol>();
			for (int j=0;j<k*(k-1)/3;j++) { //Tiers des trajets possible effectues chaque jour (k aeroports de depart possible, k-1 aeroports d'arrivee possibles)
				n = (int)(Math.random() * k); //Pour selectionner aleatoirement dans la liste des aeroports, l'aeroport de depart
				ae1=lst_ae.get(n);
				z = ae1.getAeroports_desservis().size();
				m = (int)(Math.random() * z); //Pour selectionner aleatoirement dans la liste ds aeroports desservi, l'aeroport d'arrivee
				ae2=ae1.getAeroports_desservis().get(m);
				while (contient(ae1,ae2,lst_volJour)) {
					n = (int)(Math.random() * (k));
					ae1=lst_ae.get(n);
					z = ae1.getAeroports_desservis().size();
					m = (int)(Math.random() * z);
					ae2=ae1.getAeroports_desservis().get(m);
				}
				id =ae1.getId()+ae2.getId();
				Vol v = new Vol(id,0,ae1,ae2,null,new MyDate(d.getJour(),d.getMois(),d.getAnnee()));

				lst_volJour.add(v);	
				lst_volSemaine.add(v);
			}
			d=d.demain(); //On passe au jour suivant quand on a place les vols d'un jour
		}
		return lst_volSemaine;
	}
	
	/**
	 * Cette fonction cree une reservation sur un des vols d une liste de vol fournie en parametre
	 * 
	 * @param lst_vol est la liste des vols de la semaine suivante
	 * @param cpt permet de modifier l id de la resservation
	 * @return la reservation complete
	 */
	
	public static Reservation chargerResa(ArrayList<Vol> lst_vol,int cpt) {
		int k = lst_vol.size();
		int n = (int)(Math.random() * k);
		
		
		Aeroport ae1=lst_vol.get(n).getAe_depart();
		Aeroport ae2=lst_vol.get(n).getAe_arrivee();
		
		String id = "resa"+ae1.getId()+ae2.getId()+cpt;
		
		int nb;
		double p = Math.random();
		if (p<0.6) {nb=1;}
		else if (p<0.85) {nb=6;}
		else {nb = 11;}
		nb+=(int)(Math.random() * 5);
		Reservation resa = new Reservation(id,lst_vol.get(n).getDate(),ae1,ae2,nb);
		lst_vol.get(n).setNb_passagers(lst_vol.get(n).getNb_passagers()+nb);
		return resa;
	}
	
public static ArrayList<Aeroport> chargerAe() {
		
		//Creation d aeroports aux bonnes coordonnees
		
		ArrayList<Aeroport> lst_ae = new ArrayList<Aeroport>();
		Coordonnees coor1 = new Coordonnees(49.0097,2.5479);
		Coordonnees coor2 = new Coordonnees(35.6895,139.6917);
		Coordonnees coor3 = new Coordonnees(51.5112,-0.1198);
		Coordonnees coor4 = new Coordonnees(40.7144,-74.006);
		Coordonnees coor5 = new Coordonnees(-22.9201,-43.3307);
		Coordonnees coor6 = new Coordonnees(33.5204,151.1226);
		Coordonnees coor7 = new Coordonnees(52.52437,13.41053);
		Aeroport CDG = new Aeroport();
		CDG.setId("PAR");
		CDG.setCoor(coor1);
		Aeroport TOK = new Aeroport();
		TOK.setId("TOK");
		TOK.setCoor(coor2);
		Aeroport LON = new Aeroport();
		LON.setId("LON");
		LON.setCoor(coor3);
		Aeroport NY = new Aeroport();
		NY.setId("NY");
		NY.setCoor(coor4);
		Aeroport RIO = new Aeroport();
		RIO.setId("RIO");
		RIO.setCoor(coor5);
		Aeroport SYD = new Aeroport();
		SYD.setId("SYD");
		SYD.setCoor(coor6);
		Aeroport BER = new Aeroport();
		BER.setId("BER");
		BER.setCoor(coor7);
		lst_ae.add(CDG);
		lst_ae.add(TOK);
		lst_ae.add(LON);
		lst_ae.add(NY);
		lst_ae.add(RIO);
		lst_ae.add(SYD);
		lst_ae.add(BER);
		
		//Avions crees et places dans les aeroports
		
		ArrayList<Avion> lst_avCDG = new ArrayList<Avion>();
		ArrayList<Avion> lst_avTOK = new ArrayList<Avion>();
		ArrayList<Avion> lst_avLON = new ArrayList<Avion>();
		ArrayList<Avion> lst_avNY = new ArrayList<Avion>();
		ArrayList<Avion> lst_avSYD = new ArrayList<Avion>();
		ArrayList<Avion> lst_avRIO = new ArrayList<Avion>();
		ArrayList<Avion> lst_avBER = new ArrayList<Avion>();
		
		lst_avCDG.add(new A380("A380_1"));
		lst_avCDG.add(new A380("A380_2"));
		lst_avCDG.add(new A320("A320_1"));
		lst_avCDG.add(new A330("A330_1"));
		lst_avCDG.add(new Boeing707("Boeing707_1"));
		lst_avCDG.add(new Boeing727("Boeing727_1"));
		lst_avCDG.add(new Boeing747("Boeing747_1"));
		lst_avCDG.add(new Boeing767("Boeing767_1"));
		lst_avCDG.add(new Concorde("Concorde_1"));
		lst_avCDG.add(new Embraer195("Embraer195_1"));
		
		lst_avTOK.add(new A380("A380_4"));
		lst_avTOK.add(new A380("A380_5"));
		lst_avTOK.add(new A320("A320_2"));
		lst_avTOK.add(new A330("A330_2"));
		lst_avTOK.add(new Boeing707("Boeing707_2"));
		lst_avTOK.add(new Boeing727("Boeing727_2"));
		lst_avTOK.add(new Boeing747("Boeing747_2"));
		lst_avTOK.add(new Boeing767("Boeing767_2"));
		lst_avTOK.add(new Concorde("Concorde_2"));
		lst_avTOK.add(new Embraer195("Embraer195_2"));
		
		lst_avLON.add(new A380("A380_7"));
		lst_avLON.add(new A380("A380_8"));
		lst_avLON.add(new A380("A380_9"));
		lst_avLON.add(new A320("A320_3"));
		lst_avLON.add(new A330("A330_3"));
		lst_avLON.add(new Boeing707("Boeing707_3"));
		lst_avLON.add(new Boeing727("Boeing727_3"));
		lst_avLON.add(new Boeing747("Boeing747_3"));
		lst_avLON.add(new Boeing767("Boeing767_3"));
		lst_avLON.add(new Concorde("Concorde_3"));
		lst_avLON.add(new Embraer195("Embraer195_3"));
		
		lst_avNY.add(new A380("A380_10"));
		lst_avNY.add(new A380("A380_11"));
		lst_avNY.add(new A320("A320_4"));
		lst_avNY.add(new A330("A330_4"));
		lst_avNY.add(new Boeing707("Boeing707_4"));
		lst_avNY.add(new Boeing727("Boeing727_4"));
		lst_avNY.add(new Boeing747("Boeing747_4"));
		lst_avNY.add(new Boeing767("Boeing767_4"));
		lst_avNY.add(new Concorde("Concorde_4"));
		lst_avNY.add(new Embraer195("Embraer195_4"));
		
		lst_avRIO.add(new A380("A380_13"));
		lst_avRIO.add(new A380("A380_14"));
		lst_avRIO.add(new A320("A320_5"));
		lst_avRIO.add(new A330("A330_5"));
		lst_avRIO.add(new Boeing707("Boeing707_5"));
		lst_avRIO.add(new Boeing727("Boeing727_5"));
		lst_avRIO.add(new Boeing747("Boeing747_5"));
		lst_avRIO.add(new Boeing767("Boeing767_5"));
		lst_avRIO.add(new Concorde("Concorde_5"));
		lst_avRIO.add(new Embraer195("Embraer195_5"));
		
		lst_avSYD.add(new A380("A380_16"));
		lst_avSYD.add(new A380("A380_17"));
		lst_avSYD.add(new A320("A320_6"));
		lst_avSYD.add(new A330("A330_6"));
		lst_avSYD.add(new Boeing707("Boeing707_6"));
		lst_avSYD.add(new Boeing727("Boeing727_6"));
		lst_avSYD.add(new Boeing747("Boeing747_6"));
		lst_avSYD.add(new Boeing767("Boeing767_6"));
		lst_avSYD.add(new Concorde("Concorde_6"));
		lst_avSYD.add(new Embraer195("Embraer195_6"));
		
		lst_avBER.add(new A380("A380_19"));
		lst_avBER.add(new A380("A380_20"));
		lst_avBER.add(new A320("A320_7"));
		lst_avBER.add(new A330("A330_7"));
		lst_avBER.add(new Boeing707("Boeing707_7"));
		lst_avBER.add(new Boeing727("Boeing727_7"));
		lst_avBER.add(new Boeing747("Boeing747_7"));
		lst_avBER.add(new Boeing767("Boeing767_7"));
		lst_avBER.add(new Concorde("Concorde_7"));
		lst_avBER.add(new Embraer195("Embraer195_7"));
		
		CDG.setAvions_presents(lst_avCDG);
		TOK.setAvions_presents(lst_avTOK);
		LON.setAvions_presents(lst_avLON);
		NY.setAvions_presents(lst_avNY);
		RIO.setAvions_presents(lst_avRIO);
		SYD.setAvions_presents(lst_avSYD);
		BER.setAvions_presents(lst_avBER);

		//Aeroports desservis
		
		ArrayList<Aeroport> lst_aeCDG = new ArrayList<Aeroport>();
		lst_aeCDG.add(TOK);
		lst_aeCDG.add(LON);
		lst_aeCDG.add(NY);
		lst_aeCDG.add(RIO);
		lst_aeCDG.add(SYD);
		lst_aeCDG.add(BER);
		CDG.setAeroports_desservis(lst_aeCDG);
		ArrayList<Aeroport> lst_aeTOK = new ArrayList<Aeroport>();
		lst_aeTOK.add(CDG);
		lst_aeTOK.add(LON);
		lst_aeTOK.add(NY);
		lst_aeCDG.add(RIO);
		lst_aeCDG.add(SYD);
		lst_aeCDG.add(BER);
		TOK.setAeroports_desservis(lst_aeTOK);
		ArrayList<Aeroport> lst_aeLON = new ArrayList<Aeroport>();
		lst_aeLON.add(TOK);
		lst_aeLON.add(CDG);
		lst_aeLON.add(NY);
		//lst_aeLON.add(RIO);
		lst_aeLON.add(SYD);
		lst_aeLON.add(BER);
		LON.setAeroports_desservis(lst_aeLON);
		ArrayList<Aeroport> lst_aeNY = new ArrayList<Aeroport>();
		lst_aeNY.add(TOK);
		lst_aeNY.add(LON);
		lst_aeNY.add(CDG);
		lst_aeNY.add(RIO);
		lst_aeNY.add(SYD);
		lst_aeNY.add(BER);
		NY.setAeroports_desservis(lst_aeNY);
		ArrayList<Aeroport> lst_aeRIO = new ArrayList<Aeroport>();
		//lst_aeRIO.add(TOK);
		lst_aeRIO.add(LON);
		lst_aeRIO.add(CDG);
		lst_aeRIO.add(NY);
		//lst_aeRIO.add(SYD);
		lst_aeRIO.add(BER);
		RIO.setAeroports_desservis(lst_aeRIO);
		ArrayList<Aeroport> lst_aeSYD = new ArrayList<Aeroport>();
		lst_aeSYD.add(TOK);
		lst_aeSYD.add(LON);
		lst_aeSYD.add(CDG);
		lst_aeSYD.add(NY);
		//lst_aeSYD.add(RIO);
		lst_aeSYD.add(BER);
		SYD.setAeroports_desservis(lst_aeSYD);
		ArrayList<Aeroport> lst_aeBER = new ArrayList<Aeroport>();
		lst_aeBER.add(TOK);
		lst_aeBER.add(LON);
		lst_aeBER.add(CDG);
		lst_aeBER.add(NY);
		lst_aeBER.add(RIO);
		lst_aeBER.add(SYD);
		BER.setAeroports_desservis(lst_aeBER);
		
		return lst_ae;
	}

}
