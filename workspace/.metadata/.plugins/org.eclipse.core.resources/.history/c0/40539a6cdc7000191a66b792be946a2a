package rouge;

//Imports

import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.Math;
import orange.*;

/**
 * @author Thibault
 * 
 */

public class Aeroport {
	
	//Declaration des attributs
	
	private String id;
	private ArrayList<Aeroport> aeroports_desservis;
	private ArrayList<Avion> avions_presents;
	private Coordonnees coor;
 
	
	//Constructeurs
	public Aeroport() {
	}

	public Aeroport(String nom, ArrayList<Aeroport> lst_ae, ArrayList<Avion> lst_av,Coordonnees coordonnes) {
		this.setId(nom);
		this.setAeroports_desservis(lst_ae);
		this.setAvions_presents(lst_av);
		this.setCoor(coordonnes);
	}
	
	
	//Getter et Setter
	
	public ArrayList<Aeroport> getAeroports_desservis() {
		return aeroports_desservis;
	}

	public void setAeroports_desservis(ArrayList<Aeroport> aeroports_desservis) {
		this.aeroports_desservis = aeroports_desservis;
	}

	public ArrayList<Avion> getAvions_presents() {
		return avions_presents;
	}

	public void setAvions_presents(ArrayList<Avion> avions_presents) {
		this.avions_presents = avions_presents;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public Coordonnees getCoor() {
		return coor;
	}

	public void setCoor(Coordonnees coor) {
		this.coor = coor;
	}
	
	
	//Methodes
	
	/**
	 * @ param Aeroport ae_dep et ae_arrivee : Les aeroports de depart et arrivee 
	 * @return la distance entre ces deux aeroports
	 * 
	 * Methode qui calcule la distance entre deux aeroports, nous donnera la distance en km des vols.
	 */
	
	public static double calcul_distance(Aeroport ae_depart,Aeroport ae_arrivee) {
		double lat1=ae_depart.getCoor().getLat() * Math.PI/180;
		double lon1=ae_depart.getCoor().getLon() * Math.PI/180;
		double lat2=ae_arrivee.getCoor().getLat() * Math.PI/180;
		double lon2=ae_arrivee.getCoor().getLon() * Math.PI/180;
		double R_terre = 6371;
		double dist = Math.acos(Math.sin(lat1)*Math.sin(lat2)+Math.cos(lat1)*Math.cos(lat2)*(Math.cos(lon2-lon1)))*R_terre;
		//Formule de Vicenty (Geodesie)
		return dist;
	}
	
	public static ArrayList<Aeroport> chargerTest() {
		ArrayList<Aeroport> lst_ae = new ArrayList<Aeroport>();
		Coordonnees coor1 = new Coordonnees(49.0097,2.5479);
		Coordonnees coor2 = new Coordonnees(35.6895,139.6917);
		Coordonnees coor3 = new Coordonnees(51.5112,-0.1198);
		Coordonnees coor4 = new Coordonnees(40.7144,-74.006);
		Aeroport CDG = new Aeroport();
		CDG.setId("CDG");
		CDG.setCoor(coor1);
		Aeroport TOK = new Aeroport();
		TOK.setId("TOK");
		TOK.setCoor(coor2);
		Aeroport LON = new Aeroport();
		LON.setId("LON");
		LON.setCoor(coor3);
		Aeroport NY = new Aeroport();
		NY.setId("NY");
		NY.setCoor(coor4);
		lst_ae.add(CDG);
		lst_ae.add(TOK);
		lst_ae.add(LON);
		lst_ae.add(NY);
		
		ArrayList<Avion> lst_avCDG = new ArrayList<Avion>();
		ArrayList<Avion> lst_avTOK = new ArrayList<Avion>();
		ArrayList<Avion> lst_avLON = new ArrayList<Avion>();
		ArrayList<Avion> lst_avNY = new ArrayList<Avion>();
		Avion av1 = new A380("av1");
		Avion av2 = new A380("av2");
		Avion av3 = new A320("av3");
		Avion av4 = new A320("av4");
		Avion av5 = new A320("av5");
		Avion av6 = new A320("av6");
		Avion av7 = new Concorde("av7");
		Avion av8 = new Concorde("av8");
		Avion av9 = new Boeing727("av9");
		Avion av10 = new Boeing727("av10");
		lst_avCDG.add(av1);
		lst_avCDG.add(av6);
		lst_avCDG.add(new A380("av11"));
		lst_avCDG.add(new A380("av12"));
		lst_avCDG.add(new A320("av13"));
		lst_avCDG.add(new A320("av14"));
		lst_avCDG.add(new A320("av15"));
		lst_avCDG.add(av3);
		lst_avTOK.add(av5);
		lst_avTOK.add(av8);
		lst_avTOK.add(new A380("av21"));
		lst_avTOK.add(new A380("av22"));
		lst_avTOK.add(new A320("av23"));
		lst_avTOK.add(new A320("av24"));
		lst_avTOK.add(new A320("av25"));
		lst_avTOK.add(av7);
		lst_avLON.add(av2);
		lst_avLON.add(av9);
		lst_avLON.add(new A380("av31"));
		lst_avLON.add(new A380("av32"));
		lst_avLON.add(new A320("av33"));
		lst_avLON.add(new A320("av34"));
		lst_avLON.add(new A320("av35"));
		lst_avNY.add(av4);
		lst_avNY.add(av10);
		lst_avNY.add(new A380("av41"));
		lst_avNY.add(new A380("av42"));
		lst_avNY.add(new A320("av43"));
		lst_avNY.add(new A320("av44"));
		lst_avNY.add(new A320("av45"));
		CDG.setAvions_presents(lst_avCDG);
		TOK.setAvions_presents(lst_avTOK);
		LON.setAvions_presents(lst_avLON);
		NY.setAvions_presents(lst_avNY);

		ArrayList<Aeroport> lst_aeCDG = new ArrayList<Aeroport>();
		lst_aeCDG.add(TOK);
		lst_aeCDG.add(LON);
		lst_aeCDG.add(NY);
		CDG.setAeroports_desservis(lst_aeCDG);
		ArrayList<Aeroport> lst_aeTOK = new ArrayList<Aeroport>();
		lst_aeTOK.add(CDG);
		lst_aeTOK.add(LON);
		lst_aeTOK.add(NY);
		TOK.setAeroports_desservis(lst_aeTOK);
		ArrayList<Aeroport> lst_aeLON = new ArrayList<Aeroport>();
		lst_aeLON.add(TOK);
		lst_aeLON.add(CDG);
		lst_aeLON.add(NY);
		LON.setAeroports_desservis(lst_aeLON);
		ArrayList<Aeroport> lst_aeNY = new ArrayList<Aeroport>();
		lst_aeNY.add(TOK);
		lst_aeNY.add(LON);
		lst_aeNY.add(CDG);
		NY.setAeroports_desservis(lst_aeNY);
		
		return lst_ae;
	}
	
	
	
	
	public static ArrayList<Aeroport> chargerAe(String chemin) {
		ArrayList<Aeroport> lst_ae = new ArrayList<Aeroport>();
		Aeroport ae = new Aeroport();
		File file = new File(chemin);
		Scanner scan = null;
		try {
			scan = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while (scan.hasNextLine()) {
			ae.setId(scan.toString());
			ae.setCoor(new Coordonnees(scan.nextInt(),scan.nextInt()));
			lst_ae.add(ae);
		}
		return lst_ae;
	}
	
	public static void chargerLiaison(ArrayList<Aeroport> lst_ae, String chemin) {
		//On lit le fichier de liaison. --> devient une liste de (id,id)
		ArrayList<ArrayList<String>> fichier = new ArrayList<ArrayList<String>>();
		for (Aeroport ae:lst_ae) {
			for(Aeroport ae2 : lst_ae) {
				for (int i=0; i< fichier.size(); i++) {
					if (fichier.get(i).get(0)==ae.getId() && fichier.get(i).get(1)==ae2.getId()) {
						ae.getAeroports_desservis().add(ae2);
						ae2.getAeroports_desservis().add(ae);
					}
				}
			}
		}
	}
	
}
