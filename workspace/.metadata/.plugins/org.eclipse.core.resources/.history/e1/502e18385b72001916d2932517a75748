package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import avion.A320;
import avion.A380;
import avion.Avion;
import avion.Boeing727;
import avion.Concorde;
import batiments.Aeroport;
import batiments.Coordonnees;
import gestion.Reservation;
import gestion.Vol;
import myDate.MyDate;

public class Chargement {
	/**
	 * Cette methode permet de verifier qu'un vol s'effectue bien entre deux aeroports donnes
	 * 
	 * @param ae1 l'aeroport theorique de depart
	 * @param ae2 l'aeroport theorique d'arrivee
	 * @param lst la liste des vols a verifier
	 * @return true si on a bien les bons aeroports de depart et d'arrivee
	 */
	
	public static boolean contient(Aeroport ae1,Aeroport ae2,ArrayList<Vol> lst) {
		for (Vol v : lst) {
			if (v.getAe_depart().equals(ae1) && v.getAe_arrivee().equals(ae2)) {return true;}
		}
		return false;
	}
	
	/**
	 * 
	 * Cetter methode permet de creer l'ensemble des vols d'une semaine type
	 * 
	 * @param lst_ae
	 * @param date
	 * @return la liste des vols qui seront effectues dans la semaine
	 */
	
	public static ArrayList<Vol> creerVol(ArrayList<Aeroport> lst_ae,MyDate date) {
		ArrayList<Vol> lst_volSemaine = new ArrayList<Vol>();
		Aeroport ae1 = null,ae2 = null;
		String id;
		MyDate d = new MyDate(date.getJour(),date.getMois(),date.getAnnee());
		int n,m,z;
		int k = lst_ae.size(); //Nombre d'aeroports
		for (int i=0;i<7;i++) {
			ArrayList<Vol> lst_volJour = new ArrayList<Vol>();
			for (int j=0;j<k*(k-1)/2;j++) { //Moitie des trajets possible effectues chaque jour (k aeroports de depart possible, k-1 aeroports d'arrivee possibles)
				n = (int)(Math.random() * k); //Pour selectionner aleatoirement dans la liste des aeroports, l'aeroport de depart
				ae1=lst_ae.get(n);
				z = ae1.getAeroports_desservis().size();
				m = (int)(Math.random() * z); //Pour selectionner aleatoirement dans la liste ds aeroports desservi, l'aeroport d'arrivee
				ae2=ae1.getAeroports_desservis().get(m);
				while (contient(ae1,ae2,lst_volJour)) {
					n = (int)(Math.random() * (k));
					ae1=lst_ae.get(n);
					z = ae1.getAeroports_desservis().size();
					m = (int)(Math.random() * z);
					ae2=ae1.getAeroports_desservis().get(m);
				}
				id =ae1.getId()+ae2.getId();
				Vol v = new Vol(id,0,ae1,ae2,null,new MyDate(d.getJour(),d.getMois(),d.getAnnee()));

				lst_volJour.add(v);	
				lst_volSemaine.add(v);
			}
			d=d.demain(); //On passe au jour suivant quand on a place les vols d'un jour
		}
		return lst_volSemaine;
	}
	
	/**
	 * Cette fonction cree une reservation sur un des vols d une liste de vol fournie en parametre
	 * 
	 * @param lst_vol est la liste des vols de la semaine suivante
	 * @param cpt permet de modifier l id de la resservation
	 * @return la reservation complete
	 */
	
	public static Reservation chargerResa(ArrayList<Vol> lst_vol,int cpt) {
		int k = lst_vol.size();
		int n = (int)(Math.random() * k);
		
		
		Aeroport ae1=lst_vol.get(n).getAe_depart();
		Aeroport ae2=lst_vol.get(n).getAe_arrivee();
		
		String id = "resa"+ae1.getId()+ae2.getId()+cpt;
		
		int nb;
		double p = Math.random();
		if (p<0.6) {nb=1;}
		else if (p<0.85) {nb=6;}
		else {nb = 11;}
		nb+=(int)(Math.random() * 5);
		Reservation resa = new Reservation(id,lst_vol.get(n).getDate(),ae1,ae2,nb);
		lst_vol.get(n).setNb_passagers(lst_vol.get(n).getNb_passagers()+nb);
		return resa;
	}
	
public static ArrayList<Aeroport> chargerTest() {
		
		//Creation d aeroports aux bonnes coordonnees
		
		ArrayList<Aeroport> lst_ae = new ArrayList<Aeroport>();
		Coordonnees coor1 = new Coordonnees(49.0097,2.5479);
		Coordonnees coor2 = new Coordonnees(35.6895,139.6917);
		Coordonnees coor3 = new Coordonnees(51.5112,-0.1198);
		Coordonnees coor4 = new Coordonnees(40.7144,-74.006);
		Aeroport CDG = new Aeroport();
		CDG.setId("CDG");
		CDG.setCoor(coor1);
		Aeroport TOK = new Aeroport();
		TOK.setId("TOK");
		TOK.setCoor(coor2);
		Aeroport LON = new Aeroport();
		LON.setId("LON");
		LON.setCoor(coor3);
		Aeroport NY = new Aeroport();
		NY.setId("NY");
		NY.setCoor(coor4);
		lst_ae.add(CDG);
		lst_ae.add(TOK);
		lst_ae.add(LON);
		lst_ae.add(NY);
		
		//Avions crees et places dans les aeroports
		
		ArrayList<Avion> lst_avCDG = new ArrayList<Avion>();
		ArrayList<Avion> lst_avTOK = new ArrayList<Avion>();
		ArrayList<Avion> lst_avLON = new ArrayList<Avion>();
		ArrayList<Avion> lst_avNY = new ArrayList<Avion>();
		Avion av1 = new A380("av1");
		Avion av2 = new A380("av2");
		Avion av3 = new A320("av3");
		Avion av4 = new A320("av4");
		Avion av5 = new A320("av5");
		Avion av6 = new A320("av6");
		Avion av7 = new Concorde("av7");
		Avion av8 = new Concorde("av8");
		Avion av9 = new Boeing727("av9");
		Avion av10 = new Boeing727("av10");
		lst_avCDG.add(av1);
		lst_avCDG.add(av6);
		lst_avCDG.add(new A380("av11"));
		lst_avCDG.add(new A380("av12"));
		lst_avCDG.add(new A380("av13"));
		lst_avCDG.add(new A320("av14"));
		lst_avCDG.add(new A320("av15"));
		lst_avCDG.add(av3);
		lst_avTOK.add(av5);
		lst_avTOK.add(av8);
		lst_avTOK.add(new A380("av21"));
		lst_avTOK.add(new A380("av22"));
		lst_avTOK.add(new A380("av23"));
		lst_avTOK.add(new A320("av24"));
		lst_avTOK.add(new A320("av25"));
		lst_avTOK.add(av7);
		lst_avLON.add(av2);
		lst_avLON.add(av9);
		lst_avLON.add(new A380("av31"));
		lst_avLON.add(new A380("av32"));
		lst_avLON.add(new A380("av33"));
		lst_avLON.add(new A320("av34"));
		lst_avLON.add(new A320("av35"));
		lst_avNY.add(av4);
		lst_avNY.add(av10);
		lst_avNY.add(new A380("av41"));
		lst_avNY.add(new A380("av42"));
		lst_avNY.add(new A380("av43"));
		lst_avNY.add(new A320("av44"));
		lst_avNY.add(new A320("av45"));
		CDG.setAvions_presents(lst_avCDG);
		TOK.setAvions_presents(lst_avTOK);
		LON.setAvions_presents(lst_avLON);
		NY.setAvions_presents(lst_avNY);

		//Aeroports desservis
		
		ArrayList<Aeroport> lst_aeCDG = new ArrayList<Aeroport>();
		lst_aeCDG.add(TOK);
		lst_aeCDG.add(LON);
		lst_aeCDG.add(NY);
		CDG.setAeroports_desservis(lst_aeCDG);
		ArrayList<Aeroport> lst_aeTOK = new ArrayList<Aeroport>();
		lst_aeTOK.add(CDG);
		lst_aeTOK.add(LON);
		lst_aeTOK.add(NY);
		TOK.setAeroports_desservis(lst_aeTOK);
		ArrayList<Aeroport> lst_aeLON = new ArrayList<Aeroport>();
		lst_aeLON.add(TOK);
		lst_aeLON.add(CDG);
		lst_aeLON.add(NY);
		LON.setAeroports_desservis(lst_aeLON);
		ArrayList<Aeroport> lst_aeNY = new ArrayList<Aeroport>();
		lst_aeNY.add(TOK);
		lst_aeNY.add(LON);
		lst_aeNY.add(CDG);
		NY.setAeroports_desservis(lst_aeNY);
		
		return lst_ae;
	}

}
