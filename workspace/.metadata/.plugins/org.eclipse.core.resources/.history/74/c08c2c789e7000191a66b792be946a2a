package vert;

//Imports
import rouge.*;
import jaune.*;
import mydate.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


/**
 * 
 * @author Aurore et Thibault 
 * Cette classe permet de gerer les reservations des clients en trouvant un trajet et fournissant un recapitulatif
 *
 */

public class Reservation {
		
	//Declaration des attributs
	
	private String id;
	private MyDate date;
	private Aeroport aeDep;
	private Aeroport aeArr;
	private int nbPassagers;
	private ArrayList<Vol> lst_vol;
	
	//Constructeur 
	public Reservation(String id, MyDate date, Aeroport aeDep, Aeroport aeArr, int nbPassagers) {
		this.id=id;
		this.date=date;
		this.aeDep=aeDep;
		this.aeArr=aeArr;
		this.nbPassagers=nbPassagers;
	}
	
	public Reservation(String id, MyDate date, Aeroport aeDep, Aeroport aeArr, int nbPassagers, ArrayList<Vol> lst_vol) {
		this.id=id;
		this.date=date;
		this.aeDep=aeDep;
		this.aeArr=aeArr;
		this.nbPassagers=nbPassagers;
		this.setLst_vol(lst_vol);
	}
	
	//Getter et Setter
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public MyDate getDate() {
		return date;
	}

	public void setDate(MyDate date) {
		this.date = date;
	}

	public Aeroport getaeDep() {
		return aeDep;
	}

	public void setaeDep(Aeroport aeDep) {
		this.aeDep = aeDep;
	}

	public Aeroport getaeArr() {
		return aeArr;
	}

	public void setaeArr(Aeroport aeArr) {
		this.aeArr = aeArr;
	}

	public int getNbPassagers() {
		return nbPassagers;
	}

	public void setNbPassagers(int nbPassagers) {
		this.nbPassagers = nbPassagers;
	}
	
	public ArrayList<Vol> getLst_vol() {
		return lst_vol;
	}

	public void setLst_vol(ArrayList<Vol> lst_vol) {
		this.lst_vol = lst_vol;
	}
	
	//Methodes
	
	/**
	 * 
	 * Cette methode permet de trouver des vols pour constituer un trajet entre l'aeroport de depart et celui d'arrivee a destination
	 * 
	 * @param listeVols , les vols existant deja entre deux aeroports
	 * @param aeDep aeroport de depart du trajet
	 * @param aeArr aeroport d arrivee du trajet 
	 * @param dateDep date de depart souhaite
	 * @param nbPassagers nombre de passagers pour lesquels la personne souhaite reserver
	 * @return la liste des vols composant le trajet
	 */
	
	public double distance(ArrayList <Aeroport> aeroports) {
		double somme = 0;
		for (int i = 0; i < aeroports.size(); i++) {
			somme += Aeroport.calcul_distance(aeroports.get(i), aeroports.get(i+1));
		}
		return somme;
	}
	
	
	
	public double cout() {
		double cout = 0;
		
		
		return cout;
	}
	
	
/**
		 * methode permettant de trouver le plus court chemin entre deux sommets
		 * C'est l'alogrithme de Dijkstra qu'on implemente ici, les aeroports etant les sommets et les vols, les arcs que l'on pondere par les distances en km
		 * @return la liste des sommets du plus court chemin
		 **/

	public ArrayList<Aeroport> findShortestPath(Aeroport start, Aeroport end) {

		ArrayList <Aeroport> visitedAeroport= new ArrayList<Aeroport>();
		Map<Aeroport,Double> distancesMin = new HashMap<Aeroport,Double>();
		Map<Aeroport,Aeroport> predecessor = new HashMap<Aeroport,Aeroport>();
		
		for(Aeroport ae : aeDep.getAeroports_desservis()) { 
			distancesMin.put(ae,Double.POSITIVE_INFINITY);
		}
		distancesMin.put(start,0.0);

		int c=0;
		while(c < aeDep.getAeroports_desservis().size()) {
			Aeroport current = null;
			double dmin = Double.POSITIVE_INFINITY;

			Set<Aeroport> cles = distancesMin.keySet();
			Iterator<Aeroport> it = cles.iterator();
			while(it.hasNext()) {
				Aeroport ae =it.next();
				if (!(visitedAeroport.contains(ae))) {
					if (distancesMin.get(ae)<dmin) {
						dmin=distancesMin.get(ae);
						current = ae;
					}
				}	
			}
			visitedAeroport.add(current);
			if (current.getId()==end.getId()) {
				break;
			}

			for (Vol v : current.getVolPossible()) {
				double distance = distancesMin.get(current) + Aeroport.calcul_distance(start, end); //Le poids est ici la distance
				Aeroport successor = v.oppositeExtremity(current);
				if (distance< distancesMin.get(successor)) {
					distancesMin.put(successor,distance);
					predecessor.put(successor,current);
				}
			}
		}
		ArrayList <Aeroport> trajet = new ArrayList<Aeroport>(); //Liste qui contient la suite d'aeroports composant le trajet
		if (predecessor.containsKey(end)) {
			Aeroport current = end;
			while(predecessor.containsKey(current)) {
				trajet.add(0,current);
				current = predecessor.get(current);
			}
		}
		trajet.add(0,start);
		return trajet;
	}
}